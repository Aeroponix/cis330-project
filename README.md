/************************************************************************
 *									*
 * Team.getName();							*
 * CIS330 -- Winter 2015						*
 * Final Project							*
 * README file								*
 *									*
 ************************************************************************/

# README #

This README file will document the creation of application, the steps 
necessary to get application up and running, and any other related 
documentation.  The members working on this program can be found in 
the contributors.txt file.


### MEMORY MATCHING GAME###

* Quick summary
* Version 1.0

In this game a deck of cards is randomly shuffled and distributed 
face down in a window.  The user clicks on a card, and it flips over. 
The user can then click on another card, and if it doesn't match, the 
first card will flip back over and the user will lose a point.  If the 
card does match, the user will earn a point for matching color, suit, 
or value (for a maximum of 2 points).  Once two cards have been matched 
and flipped over, they will no longer be clickable. The game is over 
when all the cards have been flipped over.

Within the code, comments are there to let the reader know how to operate
and maintain the program.  The comments at the beginning of the method are
relatively small, but detailed line by line comments are included in each
method for a detailed description.

### EXTRA REQUIRED LIBRARIES ###
The program requires gcc to compile and run, below are the extra 
required libraries to run/compile the program:

Required to run the main file:
 * gtk3

Required to compile the program:
 * gtk3
 * gtk3-utils
 * gtk3-devel


### TEAM MEMBERS ###
 * Abdulmajeed K.	-- 
 * Mustafa A.		-- 
 * Eric A.		-- 
 * Adam L. 		-- 


### ASSOCIATED FILES ###
 This README.md file
 card.c
 deck.c
 memory_game.c
 Makefile


### TEAM PROGRESS/MEETING NOTES ###
**NOTE:** Our group meetings were held on Thursdays at 3:30 pm in
Deschutes.  Apologies that these were added so late, I kept track of
our progress in a notebook and am just now getting to transferring
them now that I have the project working and less work in other
classes:

--WEEK 1 (FEB 1ST - 7TH):               :
This week was spent forming the group and coming up with the idea for
the final project.  We met up on thursday, decided on thursdays as our
day to meet as a group every week, and the tossed around ideas for a
final project.  We decided to make a memory game style application
(the description of which can be seen above) for it will allow us to
do a project similar to one we have done in other classes in other 
programming languages (with added level of difficulty, of course).

What to work on next is coming up with implementation ideas and ideas
for what external libraries to use for the project so that we do not
have to reinvent the window objects and on-click handlers that will 
run our game. 


--WEEK 2 (FEB 8TH - 14TH):
this week was spent finding the libraries that would help us build our
program for the final project.  We decided on Gtk+ v3.0.  It allows us
to call and create windows, buttons, and event-handlers without us as a
group having to re-invent the objects to do so.  This way we will be
able to focus on the creation of the game logic and not on the widows
that will hold the game.

For the next week we will be trying to install Gtk3 and all of its
libraries/dependencies.  We will need enough time to learn how to work all
of the features that we need, plus coding the program itself.


--WEEK 3 (FEB 15TH - 21ST):
This week was spent getting linux (Fedora v. 21) installed on several
machines so that the whole group can contribute to coding work.  Since
everyone in the group has a different computer/operating system, and it
was becoming increasingly too difficult to try figuring out how to
install the libraries and all of their dependent libraries on each
different system.  We will go with a linux operating system that will
make it easy to download and install libraries.

We have decided on Fedora v. 21 as our operating system to program on
and our next task will be to get this running for everyone and get the
libraries/dependancies installed.


--WEEK 4 (FEB 22ND - 28TH):
The majority of the program was written this week now that we all have
the same machine/operating system/libraries.  We renamed all of the
image file for the cards so that they would be optimized to call within
the code.  We then created objects for individual cards (which store the
information on idividual cards: suit, value, color, filetname) and
objects for a deck (which will hold 52 individual cards).

The next thing to do is get the game functioning with Gtk3 and get the
project finished.


--WEEK 5 (MARCH 1ST - 7TH):
This week was used to get the last of the project finished and figure
out what is not finished yet.  Need to handle exception cases such as
what happens when the same card is clicked twice, what happens when
matched cards are clicked again, etc.

Yay! the geme works, just need to spend time trying to see if we can
implement a few last features.  Also would like to try to have a 
presentation prepared and time-slot grabbed.


--WEEK 6 (MARCH 8TH - DUE DATE):








